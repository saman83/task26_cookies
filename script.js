class cookieManager {

    setCookie(cname, data) {
        let d = new Date();
        d.setTime(d.getTime() + ((2*60*60*1000)) + (15*60*1000));
        let expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + data + ";" + expires + ";path=/";
    
      }
      
      getCookie(cname) {
        let name = cname + "=";
        let decodedCookie = decodeURIComponent(document.cookie);
        let ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
          let c = ca[i];
          while (c.charAt(0) == ' ') {
            c = c.substring(1);
          }
          if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
          }
        }
        return "";
      }

      clearCookie(cname) {
        document.cookie = cname + "=;" + "expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;"
      }


}
  